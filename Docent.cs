﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._3
{
    internal class Docent
    {
        private string _naam;
        private List<Vak> _vakken;

        public Docent() 
        { 
            Vakken = new List<Vak>();
        }
        public Docent(string _naam)
        {
            Vakken = new List<Vak>();
            Naam = _naam;
        }

        public string Naam { get { return _naam; }  set { _naam = value; } }
        private List<Vak> Vakken { get { return _vakken; } set { _vakken = value; } }

        public void AddVak(Vak vak) 
        {
            bool vakBestaat = false;
            foreach (Vak v in Vakken)
            {
                if (v.Equals(vak))
                {
                    vakBestaat = true;
                    break;
                }
            }
            if (vakBestaat == false)
            {
                Vakken.Add(vak);
            }
        }
        public void RemoveVak(Vak vak) 
        {  
            Vakken.Remove(vak); 
        }
        public override string ToString()
        {
            string resultaat = string.Empty;

            if (Vakken != null)
            {
                foreach (Vak vak in Vakken)
                {
                    resultaat += vak.ToString() + Environment.NewLine;
                }
            }
            return resultaat;
        }
    }
}
