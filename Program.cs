﻿namespace Oefening_28._3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Docent dezeDocent = new Docent("Bart Nobels");
                Vak vak1 = new Vak("Basis C#", 11, "LOK1");
                Vak vak2 = new Vak("Advanced C#", 13, "LOK1");
                Vak vak3 = new Vak("Web", 12, "LOK7");
                Vak vak4 = new Vak("GIT", 2, "LOK9");
                dezeDocent.AddVak(vak1);
                dezeDocent.AddVak(vak2);
                dezeDocent.AddVak(vak3);
                dezeDocent.AddVak(vak4);

                Console.WriteLine(dezeDocent.Naam + " " + "geeft de volgende vakken:");
                Console.WriteLine(dezeDocent.ToString());
                Console.WriteLine();

                dezeDocent.RemoveVak(vak3);
                Console.WriteLine("na het verwijderen van " + vak3.Beschrijving);
                Console.WriteLine(dezeDocent.Naam + " " + "geeft de volgende vakken:");
                Console.WriteLine(dezeDocent.ToString());
            }
            catch
            {
                Console.WriteLine("Er is iets foutgelopen");
            }
        }
    }
}
