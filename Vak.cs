﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._3
{
    internal class Vak
    {
        private string _beschrijving;
        private string _leslokaal;
        private int _lesuren;

        public Vak() {}
        public Vak(string beschrijving, int lesuren, string leslokaal)
        {
            Beschrijving = beschrijving;
            Leslokaal = leslokaal;
            Lesuren = lesuren;
        }

        public string Beschrijving { get { return _beschrijving; } set { _beschrijving = value; } }
        public string Leslokaal { get {  return _leslokaal; } set { _leslokaal=value; } }
        public int Lesuren { get { return _lesuren; } set { _lesuren = value; } }

        public override bool Equals(object? obj)
        {
            bool resultaat = false;

            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Vak v = (Vak)obj;
                    if (this.Beschrijving == v.Beschrijving)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return Beschrijving + " - " + Lesuren + " - " + Leslokaal;
        }
    }
}
